import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class MovieTableModel extends AbstractTableModel {
	private final String[] columnName = 
			new String[] {"Tyty�","Re�yser","Gatunek","Rok","Ocena"};
	private List<Movie> listFilm;

	public MovieTableModel() {
		super();
		this.listFilm = new LinkedList<>();
	}

	public void addMovie(Movie m) {
		if (m != null) {
			listFilm.add(m);
			fireTableDataChanged();
		}
	}
	public void removeMovie(){
		if(!listFilm.isEmpty()){
			listFilm.remove(listFilm.size()-1);
			fireTableDataChanged();
		}
	}
	
	public void removeMovie(int index){
		if(!listFilm.isEmpty()){
			listFilm.remove(index);
			fireTableDataChanged();
		}
	}
	
@Override
public String getColumnName(int column) {
	// TODO Auto-generated method stub
	return columnName[column];
}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return listFilm.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Movie tmp = listFilm.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return tmp.getTitle();
		case 1:
			return tmp.getDirector();
		case 2:
			return tmp.getGenre();
		case 3:
			return tmp.getYear();
		case 4:
			return tmp.getRate();
		default:
			return "unknown";

		}
	}
}
