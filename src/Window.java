import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class Window {

	private JFrame frame;
	private JTextField txtTitle;
	private JTextField txtDirector;
	private JTextField txtGendre;
	private JTextField txtYear;
	private JTextField txtRating;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private MovieTableModel model;
	private int zaznaczonyIndex;
	private JButton btnRemove;

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
		model = new MovieTableModel();
		table.setModel(model);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		ListSelectionModel modelZaznaczania = table.getSelectionModel();
		modelZaznaczania.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				zaznaczonyIndex = table.getSelectedRow();
				if(zaznaczonyIndex ==-1){
					btnRemove.setEnabled(false);
				}else{
					btnRemove.setEnabled(true);
				}			
			}
		});

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 540, 356);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panelLeft = new JPanel();
		panelLeft.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panelLeft);
		GridBagLayout gbl_panelLeft = new GridBagLayout();
		gbl_panelLeft.columnWidths = new int[] { 0, 0 };
		gbl_panelLeft.rowHeights = new int[] { 0, 0, 0 };
		gbl_panelLeft.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelLeft.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		panelLeft.setLayout(gbl_panelLeft);

		JPanel panelLeftTop = new JPanel();
		GridBagConstraints gbc_panelLeftTop = new GridBagConstraints();
		gbc_panelLeftTop.insets = new Insets(0, 0, 5, 0);
		gbc_panelLeftTop.fill = GridBagConstraints.BOTH;
		gbc_panelLeftTop.gridx = 0;
		gbc_panelLeftTop.gridy = 0;
		panelLeft.add(panelLeftTop, gbc_panelLeftTop);
		GridBagLayout gbl_panelLeftTop = new GridBagLayout();
		gbl_panelLeftTop.columnWidths = new int[] { 0, 0 };
		gbl_panelLeftTop.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panelLeftTop.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelLeftTop.rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelLeftTop.setLayout(gbl_panelLeftTop);

		JLabel lblDaneFilmu = new JLabel("Dane filmu");
		GridBagConstraints gbc_lblDaneFilmu = new GridBagConstraints();
		gbc_lblDaneFilmu.insets = new Insets(0, 0, 5, 0);
		gbc_lblDaneFilmu.gridx = 0;
		gbc_lblDaneFilmu.gridy = 0;
		panelLeftTop.add(lblDaneFilmu, gbc_lblDaneFilmu);

		JPanel panelName = new JPanel();
		GridBagConstraints gbc_panelName = new GridBagConstraints();
		gbc_panelName.insets = new Insets(0, 0, 5, 0);
		gbc_panelName.fill = GridBagConstraints.BOTH;
		gbc_panelName.gridx = 0;
		gbc_panelName.gridy = 1;
		panelLeftTop.add(panelName, gbc_panelName);
		panelName.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel = new JLabel("Tytul:");
		panelName.add(lblNewLabel);

		txtTitle = new JTextField();
		panelName.add(txtTitle);
		txtTitle.setColumns(10);

		JPanel panelDirector = new JPanel();
		GridBagConstraints gbc_panelDirector = new GridBagConstraints();
		gbc_panelDirector.insets = new Insets(0, 0, 5, 0);
		gbc_panelDirector.fill = GridBagConstraints.BOTH;
		gbc_panelDirector.gridx = 0;
		gbc_panelDirector.gridy = 2;
		panelLeftTop.add(panelDirector, gbc_panelDirector);
		panelDirector.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_1 = new JLabel("Re\u017Cyser");
		panelDirector.add(lblNewLabel_1);

		txtDirector = new JTextField();
		panelDirector.add(txtDirector);
		txtDirector.setColumns(10);

		JPanel panelGendre = new JPanel();
		GridBagConstraints gbc_panelGendre = new GridBagConstraints();
		gbc_panelGendre.insets = new Insets(0, 0, 5, 0);
		gbc_panelGendre.fill = GridBagConstraints.BOTH;
		gbc_panelGendre.gridx = 0;
		gbc_panelGendre.gridy = 3;
		panelLeftTop.add(panelGendre, gbc_panelGendre);
		panelGendre.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_2 = new JLabel("Gatunek");
		panelGendre.add(lblNewLabel_2);

		txtGendre = new JTextField();
		panelGendre.add(txtGendre);
		txtGendre.setColumns(10);

		JPanel panelYear = new JPanel();
		GridBagConstraints gbc_panelYear = new GridBagConstraints();
		gbc_panelYear.insets = new Insets(0, 0, 5, 0);
		gbc_panelYear.fill = GridBagConstraints.BOTH;
		gbc_panelYear.gridx = 0;
		gbc_panelYear.gridy = 4;
		panelLeftTop.add(panelYear, gbc_panelYear);
		panelYear.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_3 = new JLabel("Rok");
		panelYear.add(lblNewLabel_3);

		txtYear = new JTextField();
		panelYear.add(txtYear);
		txtYear.setColumns(10);

		JPanel panelRaiting = new JPanel();
		GridBagConstraints gbc_panelRaiting = new GridBagConstraints();
		gbc_panelRaiting.fill = GridBagConstraints.BOTH;
		gbc_panelRaiting.gridx = 0;
		gbc_panelRaiting.gridy = 5;
		panelLeftTop.add(panelRaiting, gbc_panelRaiting);
		panelRaiting.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel_4 = new JLabel("Ocena");
		panelRaiting.add(lblNewLabel_4);

		txtRating = new JTextField();
		panelRaiting.add(txtRating);
		txtRating.setColumns(10);

		JPanel panelLeftBottom = new JPanel();
		GridBagConstraints gbc_panelLeftBottom = new GridBagConstraints();
		gbc_panelLeftBottom.fill = GridBagConstraints.BOTH;
		gbc_panelLeftBottom.gridx = 0;
		gbc_panelLeftBottom.gridy = 1;
		panelLeft.add(panelLeftBottom, gbc_panelLeftBottom);
		GridBagLayout gbl_panelLeftBottom = new GridBagLayout();
		gbl_panelLeftBottom.columnWidths = new int[] { 0, 0 };
		gbl_panelLeftBottom.rowHeights = new int[] { 0, 0, 0 };
		gbl_panelLeftBottom.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelLeftBottom.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		panelLeftBottom.setLayout(gbl_panelLeftBottom);

		JPanel panel_7 = new JPanel();
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.insets = new Insets(0, 0, 5, 0);
		gbc_panel_7.fill = GridBagConstraints.BOTH;
		gbc_panel_7.gridx = 0;
		gbc_panel_7.gridy = 0;
		panelLeftBottom.add(panel_7, gbc_panel_7);
		panel_7.setLayout(new GridLayout(1, 0, 0, 0));

		JButton btnAdd = new JButton("Dodaj");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Movie m = new Movie();
				m.setTitle(txtTitle.getText());
				m.setDirector(txtDirector.getText());
				m.setGenre(txtGendre.getText());
				m.setYear(txtYear.getText());
				m.setRate(Float.parseFloat(txtRating.getText()));
				model.addMovie(m);
				txtTitle.setText("");
				;
				txtDirector.setText("");
				txtGendre.setText("");
				txtYear.setText("");
				txtRating.setText("");
			}
		});
		panel_7.add(btnAdd);

		btnRemove = new JButton("Usu\u0144");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					model.removeMovie(zaznaczonyIndex);;
			}
	
		});
		panel_7.add(btnRemove);

		JPanel panel_8 = new JPanel();
		GridBagConstraints gbc_panel_8 = new GridBagConstraints();
		gbc_panel_8.fill = GridBagConstraints.BOTH;
		gbc_panel_8.gridx = 0;
		gbc_panel_8.gridy = 1;
		panelLeftBottom.add(panel_8, gbc_panel_8);
		panel_8.setLayout(new GridLayout(1, 0, 0, 0));

		JButton btnSave = new JButton("Zapis");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try(PrintWriter writer = new PrintWriter(new File("Dane.txt"))){
					for(Movie m: model.)
				}cach(IOException e){
					e.setStackTrace(stackTrace);
				}
			}
		});
		panel_8.add(btnSave);

		JButton btnLoad = new JButton("Odczyt");
		panel_8.add(btnLoad);

		JPanel panelRight = new JPanel();
		panel.add(panelRight);
		GridBagLayout gbl_panelRight = new GridBagLayout();
		gbl_panelRight.columnWidths = new int[] { 0, 0 };
		gbl_panelRight.rowHeights = new int[] { 0, 0 };
		gbl_panelRight.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelRight.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panelRight.setLayout(gbl_panelRight);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		panelRight.add(scrollPane, gbc_scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
